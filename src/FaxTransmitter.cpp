// hamfax -- an application for sending and receiving amateur radio facsimiles
// Copyright (C) 2001,2002
// Christof Schmitt, DH1CS <cschmitt@users.sourceforge.net>
//  
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include "Config.hpp"
#include "FaxTransmitter.hpp"
#include <cmath>

FaxTransmitter::FaxTransmitter(QObject* parent, FaxImage* faxImage)
	: QObject(parent), image(faxImage)
{
}

void FaxTransmitter::start(int sampleRate)
{
	Config& config=Config::instance();
	startLength=config.readNumEntry("/hamfax/APT/startLength");
	startFreq=config.readNumEntry("/hamfax/APT/startFrequency");
	stopLength=config.readNumEntry("/hamfax/APT/stopLength");
	stopFreq=config.readNumEntry("/hamfax/APT/stopFrequency");
	lpm=config.readNumEntry("/hamfax/fax/LPM");
	phasingLines=config.readNumEntry("/hamfax/phasing/lines");
	phaseInvers=config.readBoolEntry("/hamfax/phasing/invert");
	color=config.readBoolEntry("/hamfax/fax/color");
	mode=config.readNumEntry("/hamfax/fax/colormode");
	this->sampleRate=sampleRate;
	state=APTSTART;
	sampleNr=0;
}

void FaxTransmitter::doNext(int n)
{
	// maximum number of samples to avoid stack overrun
	n = std::min(n, 1024);

	double lineSamples=60.0*sampleRate/lpm;
	double buf[n];
	for(int i=0; i<n; i++) {
		if(state==APTSTART) {
			if(sampleNr<sampleRate*startLength) {
				buf[i]=(sampleNr*2*startFreq/sampleRate)%2;
				sampleNr++;
			} else {
				sampleNr=0;
				emit phasing();
				state=PHASING;
			}
		}
		if(state==PHASING) {
			if(sampleNr<lineSamples*phasingLines) {
				double pos=std::fmod(sampleNr,lineSamples)
					/lineSamples;
				buf[i] = (pos<0.025||pos>=0.975 )
					? (phaseInvers?0.0:1.0) 
					: (phaseInvers?1.0:0.0);
				sampleNr++;
			} else {
				state=ENDPHASING;
				sampleNr=0;
			}
		}
		if(state==ENDPHASING) {
			if(sampleNr<lineSamples) {
				buf[i]= phaseInvers?0.0:1.0;
				sampleNr++;
			} else {
				state=IMAGE;
				sampleNr=0;
				row=0;
			}
		}
		if(state==IMAGE) {
			int r, c;
			int sr;
			int rgbg;
			double pixel;
			c=static_cast<int>(std::fmod(sampleNr,lineSamples)
					   /lineSamples*cols);
			r=static_cast<int>(sampleNr/lineSamples);

			if (color) {
				switch (mode) {
					case 0: /* RGB444 */
						if (r/3 >= rows) {
							state=APTSTOP;
							sampleNr=0;
							emit aptStop();
						} else {
							rgbg = r % 3;
							buf[i] = image->getPixel(c, r/3, rgbg)/256.0;
							sampleNr++;
							if(row!=r) {
								emit imageLine((row=r)/3);
							}
						}
						break;
					case 1: /* YPbPr444 */
						if (r/3 >= rows) {
							state=APTSTOP;
							sampleNr=0;
							emit aptStop();
						} else {
							rgbg = r % 3 + 3;
							buf[i] = image->getPixel(c, r/3, rgbg)/256.0;
							sampleNr++;
							if(row!=r) {
								emit imageLine((row=r)/3);
							}
						}
						break;
					case 2: /* YPbPr422 */
						if (r % 2) {
							/* chroma line */
							if (c < cols/2) {
								rgbg = PIXEL_CB;
							} else {
								rgbg = PIXEL_CR;
								c -= cols/2;
							}
							pixel = (image->getPixel(c*2, r/2, rgbg) +
								image->getPixel(c*2 + 1, r/2, rgbg)) / 512.0;
							buf[i] = pixel;
							sampleNr++;
						} else {
							/* luma line */
							if (r/2 >= rows) {
								state=APTSTOP;
								sampleNr=0;
								emit aptStop();
							} else {
								buf[i]=image->getPixel(c, r/2, PIXEL_Y)/256.0;
								sampleNr++;
								if(row!=r) {
									emit imageLine((row=r)/2);
								}
							}
						}
						break;
					case 3: /* YPbPr420 */
						sr = r - r / 3;
						if ((r % 3 == 0) || (r % 3 == 1)) {
							/* luma line */
							if (sr >= rows) {
								state=APTSTOP;
								sampleNr=0;
								emit aptStop();
							} else {
								buf[i]=image->getPixel(c, sr, PIXEL_Y)/256.0;
								sampleNr++;
								if(row!=r) {
									row = r;
									emit imageLine(sr);
								}
							}
						} else {
							/* chroma line */
							if (c < cols/2) {
								rgbg = PIXEL_CB;
							} else {
								rgbg = PIXEL_CR;
								c -= cols/2;
							}
							pixel = (image->getPixel(c*2, sr - 1, rgbg) +
								image->getPixel(c*2 + 1, sr - 1, rgbg) +
								image->getPixel(c*2, sr - 2, rgbg) +
								image->getPixel(c*2 + 1, sr - 2, rgbg)) / 1024;
							buf[i] = pixel;
							sampleNr++;
						}
				}

			} else {
				if (r >= rows) {
					state=APTSTOP;
					sampleNr=0;
					emit aptStop();
				} else {
					buf[i]=image->getPixel(c, r, PIXEL_Y)/256.0;
					sampleNr++;
					if(row!=r) {
						emit imageLine(row=r);
					}
				}
			}
		}
		if(state==APTSTOP) {
			if(sampleNr<sampleRate*stopLength) {
				buf[i]=sampleNr*2*stopFreq/sampleRate%2;
				sampleNr++;
			} else {
				state=IDLE;
				n=i;
				emit end();
				break;
			}
		}
		if(state==IDLE) {
			n=0;
			break;
		}
	}
	emit data(buf,n);
}

void FaxTransmitter::setImageSize(int cols, int rows)
{
	this->rows=rows;
	this->cols=cols;
}

void FaxTransmitter::doAptStop(void)
{
	state=APTSTOP;
	sampleNr=0;
	emit aptStop();
}
