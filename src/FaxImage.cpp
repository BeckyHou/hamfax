// hamfax -- an application for sending and receiving amateur radio facsimiles
// Copyright (C) 2001,2002
// Christof Schmitt, DH1CS <cschmitt@users.sourceforge.net>
//  
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include "FaxImage.hpp"
#include "Config.hpp"
#include "ImageWidget.hpp"
#include <QMouseEvent>
#include <QImageWriter>
#include <unistd.h>

/* as defined in ITU-R BT.601 */
const double CM[3][3] = {
	{0.299, 0.587, 0.114},
	{-0.1687358916478555, -0.3312641083521444, 0.5},
	{0.5, -0.4186875891583452, -0.0813124108416548}};

const double CMI[3][3] = {
	{1, 0, 1.402},
	{1, -0.344136286201022, -0.714136286201022},
	{1, 1.772, 0}};

FaxImage::FaxImage(QWidget* parent)
	: QScrollArea(parent)
{
	autoScroll=Config::instance().readBoolEntry("/hamfax/GUI/autoScroll");
	setWidget(new ImageWidget(image));
}

int FaxImage::getRows(void)
{
	return image.height();
}

int FaxImage::getCols(void)
{
	return image.width();
}

double FaxImage::getPixel(int col, int row, int rgbg)
{
	if (col >= image.width())
		col = image.width() - 1;
	if (row >= image.height())
		row = image.height() - 1;
	QRgb pixel=image.pixel(col,row);
	double value;
	switch(rgbg) {
	case PIXEL_R:
		value=qRed(pixel);
		break;
	case PIXEL_G:
		value=qGreen(pixel);
		break;
	case PIXEL_B:
		value=qBlue(pixel);
		break;
	case PIXEL_Y:
		value=CM[0][0] * qRed(pixel) + CM[0][1] * qGreen(pixel) + CM[0][2] * qBlue(pixel);
		break;
	case PIXEL_CB:
		value=128.0 + CM[1][0] * qRed(pixel) + CM[1][1] * qGreen(pixel) + CM[1][2] * qBlue(pixel);
		break;
	case PIXEL_CR:
		value=128.0 + CM[2][0] * qRed(pixel) + CM[2][1] * qGreen(pixel) + CM[2][2] * qBlue(pixel);
		break;
	default:
		value = 0.0;
	};
	return value;
}

bool FaxImage::setPixel(int col, int row, double value, int rgbg)
{
	double Y, Cb, Cr;
	double r, g, b;
	if(col>=image.width() || row>=image.height()+1 || col<0 || row<0) {
		return false;
	}
	if(row>=image.height()) {
		resizeHeight(50);
	}
	QRgb oldColor=image.pixel(col,row);
	QRgb color;
	switch(rgbg) {
	case PIXEL_R:
		if (value < 0)
			value = 0;
		else if (value > 255)
			value = 255;
		color=qRgb(value,qGreen(oldColor),qBlue(oldColor));
		break;
	case PIXEL_G:
		if (value < 0)
			value = 0;
		else if (value > 255)
			value = 255;
		color=qRgb(qRed(oldColor),value,qBlue(oldColor));
		break;
	case PIXEL_B:
		if (value < 0)
			value = 0;
		else if (value > 255)
			value = 255;
		color=qRgb(qRed(oldColor),qGreen(oldColor),value);
		break;
	case PIXEL_Y:
		if (value < 0)
			value = 0;
		else if (value > 255)
			value = 255;
		color=qRgb(value,value,value);
		break;
	case PIXEL_CB:
		Y=CM[0][0] * qRed(oldColor) + CM[0][1] * qGreen(oldColor) + CM[0][2] * qBlue(oldColor);
		Cb=value;
		Cr=128.0 + CM[2][0] * qRed(oldColor) + CM[2][1] * qGreen(oldColor) + CM[2][2] * qBlue(oldColor);
		/* Leave out the zero coefficients to save CPU cycles */
		r = Y + (Cr - 128.0) * CMI[0][3];
		g = Y + (Cb - 128.0) * CMI[1][1] + (Cr - 128) * CMI[1][2];
		b = Y + (Cb - 128.0) * CMI[2][1];
		if (r > 255.0)
			r = 255;
		else if (r < 0.0)
			r = 0;
		if (g > 255.0)
			g = 255;
		else if (g < 0.0)
			g = 0;
		if (b > 255.0)
			b = 255;
		else if (b < 0.0)
			b = 0;
		color = qRgb(r, g, b);
		break;
	case PIXEL_CR:
		Y=CM[0][0] * qRed(oldColor) + CM[0][1] * qGreen(oldColor) + CM[0][2] * qBlue(oldColor);
		Cb=128.0 + CM[1][0] * qRed(oldColor) + CM[1][1] * qGreen(oldColor) + CM[1][2] * qBlue(oldColor);
		Cr=value;
		/* Leave out the zero coefficients to save CPU cycles */
		r = Y + (Cr - 128.0) * CMI[0][3];
		g = Y + (Cb - 128.0) * CMI[1][1] + (Cr - 128) * CMI[1][2];
		b = Y + (Cb - 128.0) * CMI[2][1];
		if (r > 255.0)
			r = 255;
		else if (r < 0.0)
			r = 0;
		if (g > 255.0)
			g = 255;
		else if (g < 0.0)
			g = 0;
		if (b > 255.0)
			b = 255;
		else if (b < 0.0)
			b = 0;
		color = qRgb(r, g, b);
		break;
	default:
		color = oldColor;
	};
	image.setPixel(col,row,color);
	widget()->update(col, row, 1, 1);
	if(autoScroll) {
		ensureVisible(0,row,0,0);
	}
	return true;
}

void FaxImage::resizeHeight(int h)
{
	int imageW=image.width();
	int imageH=image.height();
	image=image.copy(0,0,imageW,imageH+h);
	if(imageH+h>=1) {
		widget()->resize(imageW, imageH + h);
		if(h>0) {
			widget()->update(0, imageH, imageW, imageH + h);
		}
		if(h<0) {
			widget()->update(0, imageH + h, imageW, imageH);
		}
		emit sizeUpdated(imageW,imageH+h);
	}
}

void FaxImage::create(int cols, int rows)
{
	image = QImage(cols, rows, QImage::Format_RGB32);
	image.fill(qRgb(80,80,80));
	widget()->resize(cols, rows);
	widget()->update(0, 0, cols, rows);
	emit sizeUpdated(cols,rows);
	emit newImage();
}

void FaxImage::load(QString fileName)
{
	image=QImage(fileName).convertToFormat(QImage::Format_RGB32);
	widget()->resize(image.width(), image.height());
	widget()->update(0, 0, image.width(), image.height());
	emit sizeUpdated(image.width(),image.height());
	emit newImage();
}

bool FaxImage::save(QString fileName)
{
	QString handler;
	int n = fileName.lastIndexOf('.');

	if(n != -1) {
		QString ext = fileName.right(fileName.length() - n - 1).toLower();
		if(QImageWriter::supportedImageFormats().contains(ext.toLatin1()))
			handler = ext;
	}

	if (handler.isEmpty()) {
		// No valid extension in file name. Try PNG as default.
		fileName.append(".png");
		handler = "PNG";
	}

	return image.save(fileName,handler.toLatin1());
}

void FaxImage::scale(int width, int height)
{
	image = image.scaled(width, height, Qt::IgnoreAspectRatio,
			     Qt::SmoothTransformation);
	widget()->resize(width, height);
	widget()->update(0, 0, width, height);
	emit sizeUpdated(width,height);
	emit newImage();
}

void FaxImage::scale(int width)
{
	scale(width,width*image.height()/image.width());
}

void FaxImage::resize(int x, int y, int w, int h)
{
	if(w==0) {
		w=image.width();
	}
	if(h==0) {
		h=image.height();
	}
	image=image.copy(x, y, w, h);
	widget()->resize(w,h);
	widget()->update(0,0,w,h);
	emit sizeUpdated(w,h);
}

void FaxImage::setWidth(int w)
{
	scale(w,image.height());
}

void FaxImage::setAutoScroll(bool b)
{
	autoScroll=b;
}

void FaxImage::correctSlant(void)
{
	emit widthAdjust(static_cast<double>
			 (slant2.x()-slant1.x())/(slant1.y()-slant2.y())
			 /image.width());
}

void FaxImage::mousePressEvent(QMouseEvent* m)
{
	QPoint pos = widget()->mapFromParent(m->pos());

	slant1 = slant2;
	slant2 = pos;
	emit clicked();
}

void FaxImage::shiftColors(void)
{
	int w=image.width();
	int h=image.height();
	for(int c=0; c<w; c++) {
		for(int r=0; r<h; r++) {
			QRgb rgb=image.pixel(c,r);
			rgb=qRgb(qGreen(rgb),qBlue(rgb),qRed(rgb));
			image.setPixel(c,r,rgb);
		}
	}
	widget()->update(0, 0, w, h);
	emit newImage();
}

void FaxImage::correctBegin(void)
{
	int n=slant2.x();
	int h=image.height();
	int w=image.width();
	QImage tempImage(w, h, QImage::Format_RGB32);
	for(int c=0; c<w; c++) {
		for(int r=0; r<h; r++) {
			tempImage.setPixel(c,r,image.pixel((n+c)%w,r));
		}
	}
	image=tempImage;
	widget()->update(0, 0, w, h);
	emit newImage();
}
